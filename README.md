- ionide-vim - plugs in fsautocomplete and lsp protocol
https://github.com/ionide/Ionide-vim/

- languageclient-neovim - lsp server
https://github.com/autozimu/LanguageClient-neovim/

- deoplete - auto complete 
https://github.com/Shougo/deoplete.nvim

- ctrlsf.vim - search for text in files
https://github.com/dyng/ctrlsf.vim
 - search text
    <C-F>f <Plug>CtrlSFPrompt
    <C-F>f <Plug>CtrlSFVwordPath
    <C-F>F <Plug>CtrlSFVwordExec
    <C-F>n <Plug>CtrlSFCwordPath
    <C-F>p <Plug>CtrlSFPwordPath
    <C-F>o :CtrlSFOpen<CR>
    <C-F>t :CtrlSFToggle<CR>
    <C-F>t <Esc>:CtrlSFToggle<CR>

- fzf - search for files
https://github.com/junegunn/fzf
 - search files
    :FZF

- vim-airline - status bar
https://github.com/vim-airline/vim-airline

- tmuxline - tmux status bar
https://github.com/edkolev/tmuxline.vim

- vim-gitgutter - show git changes
https://github.com/airblade/vim-gitgutter

- neosolarized - dark and light solarized editor theme
https://github.com/iCyMind/neosolarized

- nerdtree - project explorer
https://github.com/scrooloose/nerdtree

 - vim-easy-align - line up text in bulk
https://github.com/junegunn/vim-easy-align

- nerdcommenter - comment code
https://github.com/scrooloose/nerdcommenter
 - comment
    highlight lines of text <leader> cs
 - uncomment
    <leader> cu

- ale - errors
- deoplete-ocaml - completion for ocaml
- neoformat - format the code
