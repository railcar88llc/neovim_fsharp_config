syntax on
highlight Comment gui=bold
highlight Normal gui=none
highlight NonText guibg=none
highlight Pmenu guibg=white guifg=black gui=bold

" Opaque Background (Comment out to use terminal's profile)
set termguicolors

" Transparent Background (For i3 and compton)
highlight Normal guibg=NONE ctermbg=NONE
highlight LineNr guibg=NONE ctermbg=NONE

""" Other Configurations
set tabstop=4 softtabstop=4 shiftwidth=4 expandtab smarttab autoindent
set incsearch ignorecase smartcase hlsearch
set ruler laststatus=2 showcmd showmode
set list listchars=trail:»,tab:»-
set fillchars+=vert:\ 
set wrap breakindent
set encoding=utf-8
set number
set title

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin(expand('~/.vim/plugged'))

Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'tag': '0.1.155',
    \ 'do': 'bash install.sh',
    \ }
Plug 'junegunn/fzf'
Plug 'dyng/ctrlsf.vim'

Plug 'ionide/Ionide-vim', {
    \ 'do':  'make fsautocomplete',
    \}

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }

Plug 'iCyMind/NeoSolarized'

" On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'scrooloose/nerdcommenter'

" Extras
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'edkolev/tmuxline.vim'
Plug 'airblade/vim-gitgutter'
Plug 'sbdchd/neoformat'

" Python
Plug 'Valloric/YouCompleteMe'
Plug 'vim-scripts/indentpython.vim'
Plug 'vim-syntastic/syntastic'
Plug 'zchee/deoplete-jedi'
Plug 'davidhalter/jedi-vim'
Plug 'sbdchd/neoformat'

Plug 'nvie/vim-flake8'
Plug 'petobens/poet-v'

call plug#end()

" Required for operations modifying multiple buffers like rename.
set hidden

autocmd VimEnter * NERDTree | wincmd p
colorscheme NeoSolarized
set background=dark

" general custom keymappings
ino jj <esc>
cno jj <c-c>
" Y yanks to the end of the line
nmap Y y$
" D cuts to the end of the line
nmap D d$
nmap     <C-F>f <Plug>CtrlSFPrompt
vmap     <C-F>f <Plug>CtrlSFVwordPath
vmap     <C-F>F <Plug>CtrlSFVwordExec
nmap     <C-F>n <Plug>CtrlSFCwordPath
nmap     <C-F>p <Plug>CtrlSFPwordPath
nnoremap <C-F>o :CtrlSFOpen<CR>
nnoremap <C-F>t :CtrlSFToggle<CR>
inoremap <C-F>t <Esc>:CtrlSFToggle<CR>

" ionide fsharp configuration
nnoremap <F5> :call LanguageClient_contextMenu()<CR>
" Or map each action separately
nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>
nnoremap R :call LanguageClient#textDocument_references()<CR>
nnoremap T :terminal<CR>
nnoremap F :FZF<CR>
map <Leader>] :bnext<CR>
map <Leader>[ :bprev<CR>
map <Leader>ls :buffers<CR>

let g:deoplete#enable_at_startup = 1
let g:LanguageClient_serverCommands = {
  \ 'fsharp': g:fsharp#languageserver_command
  \ }
let g:fsharp#show_signature_on_cursor_move = 1 " 0 to disable.<Paste>
let g:fsharp#automatic_workspace_init = 1 " 0 to disable.
let g:fsharp#automatic_reload_workspace = 1 " 0 to disable.
let g:fsharp#workspace_mode_peek_deep_level = 2 " default 2
if has('nvim') && exists('*nvim_open_win')
  augroup FSharpShowTooltip
    autocmd!
    autocmd CursorHold *.fs call fsharp#showTooltip()
    " tooltip delay time configured with
    set updatetime=8000
  augroup END
endif
let g:fsharp#fsi_command = "dotnet fsi"
let g:fsharp#fsi_focus_on_send = 1 " 0 to not to focus.<Paste>
let g:fsharp#fsi_keymap = "vim-fsharp"

" python config
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
let python_highlight_all=1
let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree
" disable autocompletion, because we use deoplete for completion
let g:jedi#completions_enabled = 0
" open the go-to function in split, not another buffer
let g:jedi#use_splits_not_buffers = "right"
" Enable alignment
let g:neoformat_basic_format_align = 1
" Enable tab to space conversion
let g:neoformat_basic_format_retab = 1
" Enable trimmming of trailing whitespace
let g:neoformat_basic_format_trim = 1
